# `sriov-resources` Helm chart

## Purpose

The key role of this chart for the SR-IOV network operator integration in Sylva stack is to coordinate the creation of [`SriovNetworkNodePolicy`](https://github.com/openshift/sriov-network-operator#sriovnetworknodepolicy) resources via GitOps workflows.
